About
=====

This is currently a very simple module but has ambitious aims.  Stay tuned!

Installation
============
Install the module as usual

Usage
=====
1. Add a pane to a panel using either the IPE or the default Panels UI
2.  Choose the 'radix menu style' pane
3.  Choose which menu to style, and other styling options
4.  Voila!  Your menu tree has been output with custom styling. Enjoy!